package com.example.final_movil

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.final_movil.fragments.DashBoardFragment
import com.example.final_movil.fragments.AccountFragment
import com.example.final_movil.fragments.InfoFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    private  val dashBoardFragment = DashBoardFragment()
    private  val favoriteFragment = AccountFragment()
    private  val infoFragment = InfoFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(dashBoardFragment)

        var bottom_navigation: BottomNavigationView = findViewById(R.id.botton_navigation)
        bottom_navigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.ic_dashboard -> replaceFragment(dashBoardFragment)
                R.id.ic_fav -> replaceFragment(favoriteFragment)
                R.id.ic_info -> replaceFragment(infoFragment)
            }
            true
        }
    }
    private fun replaceFragment(fragment: Fragment){
        if (fragment != null){
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container_layout,fragment)
            transaction.commit()
        }
    }
    override fun onBackPressed() {
        return
    }
}
