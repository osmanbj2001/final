package com.example.final_movil

import com.google.gson.annotations.SerializedName


data class UserModel (

    @SerializedName("message" ) var message : String?        = null,
    @SerializedName("obj"     ) var obj     : ArrayList<ObjUser> = arrayListOf(),
    @SerializedName("authToken" ) var authToken : String? = null


)
data class ObjUser (

    @SerializedName("_id"       ) var Id        : String?           = null,
    @SerializedName("firstname" ) var firstname : String?           = null,
    @SerializedName("lastname"  ) var lastname  : String?           = null,
    @SerializedName("username"  ) var username  : String?           = null,
    @SerializedName("password"  ) var password  : String?           = null,
    @SerializedName("email"     ) var email     : String?           = null,
    @SerializedName("phone"     ) var phone     : String?           = null,
    @SerializedName("favorites" ) var favorites : ArrayList<String> = arrayListOf(),
    @SerializedName("__v"       ) var _v        : Int?              = null,
    @SerializedName("houses"    ) var houses    : ArrayList<String> = arrayListOf(),
    @SerializedName("banned"    ) var banned    : Boolean?          = null

)