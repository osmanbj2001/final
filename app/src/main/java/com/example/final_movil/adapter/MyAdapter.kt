package com.example.final_movil.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.final_movil.HousesModel
import com.example.final_movil.R
import com.squareup.picasso.Picasso

class MyAdapter (var mCtx:Context, var items:List<HousesModel>):ArrayAdapter<HousesModel>(mCtx,
    R.layout.list_item,items){
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val inflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = inflater.inflate(R.layout.list_item, null)
        val imgVHouse: ImageView = view.findViewById(R.id.imageHouseL)
        val txtVTitle: TextView = view.findViewById(R.id.txtVTitleHouseL)
        val txtVPrice: TextView = view.findViewById(R.id.txtVPriceHouseL)
        val txtVSize: TextView = view.findViewById(R.id.txtVSizeHouseL)
        val txtVBath: TextView = view.findViewById(R.id.txtVBathHouseL)
        val txtVBed: TextView = view.findViewById(R.id.txtVBedHouseL)

        Picasso.get().load(items[position].obj[position].image[0]).into(imgVHouse)
        txtVTitle.text = items[position].obj[position].houseHeader
        txtVPrice.text = "$" + items[position].obj[position].price.toString()
        txtVSize.text = items[position].obj[position].terrainArea.toString()
        txtVBath.text = items[position].obj[position].bathrooms.toString()
        txtVBed.text = items[position].obj[position].bedrooms.toString()

        return view



    }
}