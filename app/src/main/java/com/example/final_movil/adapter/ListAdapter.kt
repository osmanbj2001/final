package com.example.final_movil.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.final_movil.HousesModel
import com.example.final_movil.R
import com.example.final_movil.R.*
import com.squareup.picasso.Picasso


class ListAdapter(var context: Activity,  var arrayList: ArrayList<HousesModel>): ArrayAdapter<HousesModel>(context,
    layout.list_item, arrayList) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view: View  = inflater.inflate(layout.list_item, null)
        val imgVHouse: ImageView = view.findViewById(R.id.imageHouseL)
        val txtVTitle: TextView = view.findViewById(R.id.txtVTitleHouseL)
        val txtVPrice: TextView = view.findViewById(R.id.txtVPriceHouseL)
        val txtVSize: TextView = view.findViewById(R.id.txtVSizeHouseL)
        val txtVBath: TextView = view.findViewById(R.id.txtVBathHouseL)
        val txtVBed: TextView = view.findViewById(R.id.txtVBedHouseL)

        Picasso.get().load(arrayList[position].obj[position].image[0]).into(imgVHouse)
        txtVTitle.text = arrayList[position].obj[position].houseHeader
        txtVPrice.text = arrayList[position].obj[position].price.toString()
        txtVSize.text = arrayList[position].obj[position].terrainArea.toString()
        txtVBath.text = arrayList[position].obj[position].bathrooms.toString()
        txtVBed.text = arrayList[position].obj[position].bedrooms.toString()

        return view



    }
}