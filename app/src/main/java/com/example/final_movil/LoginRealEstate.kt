package com.example.final_movil

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import org.json.JSONObject

class LoginRealEstate : AppCompatActivity() {
    private lateinit var btnLogin: Button
    private lateinit var edtuserName: EditText
    private lateinit var edtPassword: EditText
    private lateinit var txtError: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_real_estate)
        val dialogView = layoutInflater.inflate(R.layout.errodialoglogin, null)
        val dialog = AlertDialog.Builder(this).setView(dialogView).setTitle(" ").create()

        edtuserName = findViewById(R.id.edtTxtNameLogin)
        edtPassword = findViewById(R.id.edtTxtPasswordLogin)
        btnLogin = findViewById(R.id.btnLoginR)
        txtError =  dialogView.findViewById(R.id.txtErrorLogin)
        btnLogin.setOnClickListener {
            val sharedPreference =  getSharedPreferences("data", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()

            if (edtPassword.text.toString().isEmpty() || edtuserName.text.toString().isEmpty() ){
                if (edtPassword.text.toString().isEmpty()){
                    txtError.setText("Ingresa una contraseña")
                }else {
                    txtError.setText("Ingresa una usuario")
                }
                dialog.show()

            } else {
                // Disable the button itself
                it.isEnabled = false

                val url = "https://real-red-goat-yoke.cyclic.app/users/login"

                // Post parameters
                // Form fields and values
                val params = HashMap<String,String>()
                params["user"] = edtuserName.text.toString()
                params["pass"] = edtPassword.text.toString()
                val jsonObject = JSONObject(params as Map<*, *>?)

                // Volley post request with parameters
                val request = JsonObjectRequest(
                    Request.Method.POST,url,jsonObject,
                    { response ->
                        // Process the json
                        try {
                            val responseObject = Gson().fromJson(response.toString(), LoginModel::class.java)
                            Log.d("respuesta",responseObject.authToken.toString())
                            editor.putString("token", "Bearer "+responseObject.authToken.toString())
                            editor.putString("id", responseObject.obj?.Id.toString())
                            editor.commit()
                            val intent= Intent(this,MainActivity::class.java).apply {  }
                            startActivity(intent)

                        } catch (e: Exception) {
                            Log.d("Login error", e.toString())
                        }

                    }, {
                        // Error in request
//                    "Volley error: $it"
//                    Log.d("Mi2",)
                        if (it.networkResponse.statusCode == 400) {
                            txtError.text = getString(R.string.userPasswordError)

                        }else {
                            txtError.text = getString(R.string.serverError)

                        }
                        btnLogin.isEnabled = true
                        dialog.show()
                    })

                // Volley request policy, only one time request to
                // avoid duplicate transaction
                request.retryPolicy = DefaultRetryPolicy(
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                    // 0 means no retry
                    2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
                    1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
                // Add the volley post request to the request queue
                VolleySingleton.getInstance(this)
                    .addToRequestQueue(request)
            }
        }
    }
    override fun onBackPressed() {
        return
    }

}

