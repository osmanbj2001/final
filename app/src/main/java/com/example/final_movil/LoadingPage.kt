package com.example.final_movil

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer

class LoadingPage : AppCompatActivity() {
    var initialCount:Long =2000
    var countInterval:Long =500
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading_page)
        val sharedPreference =  getSharedPreferences("Onboarding", Context.MODE_PRIVATE)

        val intentLogin = Intent(this,LoginRealEstate::class.java).apply {  }
        val intentOnBoarding = Intent(this,OnBoarding::class.java).apply {  }
        object : CountDownTimer(initialCount, countInterval) {

            override fun onTick(millisUntilFinished: Long) {
                var actual:Long =millisUntilFinished/1000
            }

            override fun onFinish() {
                if(sharedPreference.getBoolean("complete", false)){
                    startActivity(intentLogin)
                }else{
                    startActivity(intentOnBoarding)
                }

            }
        }.start()
    }
}