package com.example.final_movil

import com.google.gson.annotations.SerializedName


data class HousesModel (

    @SerializedName("message" ) var message : String?        = null,
    @SerializedName("obj"     ) var obj     : ArrayList<Obj> = arrayListOf()

)

data class Obj (

    @SerializedName("_id"               ) var Id                : String?                      = null,
    @SerializedName("ownerName"         ) var ownerName         : String?                      = null,
    @SerializedName("ownerEmail"        ) var ownerEmail        : String?                      = null,
    @SerializedName("ownerPhone"        ) var ownerPhone        : String?                      = null,
    @SerializedName("houseHeader"       ) var houseHeader       : String?                      = null,
    @SerializedName("description"       ) var description       : String?                      = null,
    @SerializedName("address"           ) var address           : String?                      = null,
    @SerializedName("location"          ) var location          : Location?                    = Location(),
    @SerializedName("image"             ) var image             : ArrayList<String>            = arrayListOf(),
    @SerializedName("dealType"          ) var dealType          : String?                      = null,
    @SerializedName("price"             ) var price             : Double?                         = null,
    @SerializedName("buildingType"      ) var buildingType      : String?                      = null,
    @SerializedName("availability"      ) var availability      : String?                      = null,
    @SerializedName("extraConstruction" ) var extraConstruction : ArrayList<ExtraConstruction> = arrayListOf(),
    @SerializedName("bedrooms"          ) var bedrooms          : Double?                         = null,
    @SerializedName("bathrooms"         ) var bathrooms         : Double?                      = null,
    @SerializedName("terrainArea"       ) var terrainArea       : Double?                         = null,
    @SerializedName("buildingArea"      ) var buildingArea      : Double?                         = null,
    @SerializedName("__v"               ) var _v                : Int?                         = null

)

data class Location (

    @SerializedName("lat" ) var lat : Double? = null,
    @SerializedName("lng" ) var lng : Double? = null,
    @SerializedName("_id" ) var Id  : String? = null

)

data class ExtraConstruction (

    @SerializedName("buildingName"     ) var buildingName     : String? = null,
    @SerializedName("constructionType" ) var constructionType : String? = null,
    @SerializedName("_id"              ) var Id               : String? = null

)