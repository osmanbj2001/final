package com.example.final_movil

import android.app.DownloadManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Request.*
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import org.json.JSONObject


class Home : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        val url = "https://real-red-goat-yoke.cyclic.app/houses/find"

        // Post parameters
        // Form fields and values
        val params = HashMap<String,String>()
        params["buildingType"] = ""
        val jsonObject = JSONObject(params as Map<*, *>?)

        // Volley post request with parameters
        val request = JsonObjectRequest(
            Request.Method.POST,url, jsonObject,
            { response ->
                // Process the json
                try {
                    // Toast.makeText(this, "${response["message"]}", Toast.LENGTH_LONG).show()
                    val responseObject = Gson().fromJson(response.toString(), HousesModel::class.java)

                    Log.d("value", responseObject.message.toString())
                    Log.d("value", responseObject.obj[0].location?.lat.toString())
                    Log.d("value", responseObject.obj[0].location?.lng.toString())
                    Log.d("value", responseObject.obj[0].ownerName.toString())

                    for (house in responseObject.obj) {
                    Log.d("value",house.location?.lat.toString())
                    Log.d("value",house.location?.lng.toString())
                    }

                } catch (e: Exception) {
                    Log.d("Mi2","${e.message}")

                    Toast.makeText(this, "Hubo algún error", Toast.LENGTH_LONG).show()
                }

            }, {
                // Error in request
//                    "Volley error: $it"
                Log.d("Mi2","$it")
            })


        // Volley request policy, only one time request to
        // avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this)
            .addToRequestQueue(request)
    }
}

