package com.example.final_movil.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.example.final_movil.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import org.json.JSONObject
import java.io.Console

class InfoFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?


    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (childFragmentManager.findFragmentById(
            R.id.map
        ) as? SupportMapFragment)?.getMapAsync { googleMap ->

            val mMap = googleMap
            val zoomLevel = 12.0f
            val direccion = LatLng(28.622839645061102, -106.11506317116468)
            mMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(direccion,zoomLevel)
            )
            getLocationCoordinates(mMap,view)

        }
    }

    fun getLocationCoordinates(map: GoogleMap, view: View)  {
        val preferences = this.requireActivity()
            .getSharedPreferences("data", Context.MODE_PRIVATE)
        val url = "https://real-red-goat-yoke.cyclic.app/houses/find"
        // Post parameters
        // Form fields and values
        val params = HashMap<String,String>()
        val jsonObject = JSONObject(params as Map<*, *>?)

        // Volley post request with parameters

        val request: JsonObjectRequest = object : JsonObjectRequest(
            Request.Method.POST,url, jsonObject,
            { response ->
                // Process the json
                try {
                    val responseObject = Gson().fromJson(response.toString(), HousesModel::class.java)

                    for (house in responseObject.obj) {
                         map.addMarker(
                            MarkerOptions()
                                .position(LatLng(house.location?.lat!!, house.location?.lng!! ))
                                .title(house.houseHeader.toString())
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_pointhouse)).anchor(0.0f,0.0f)
                        )!!
                    }
                    map.setOnMarkerClickListener { marker ->
                        val delimiter = "m"
                        var markerNum = marker.id.split(delimiter)
                        val id = responseObject.obj[markerNum[1].toInt()].Id.toString()
                        Log.d("mark", id)
                        val intent= Intent(view.context, HouseDetails::class.java).apply {  }
                        intent.putExtra("_id", id)
                        startActivity(intent)
                        //Using position get Value from arraylist
                        false
                    }
                } catch (e: Exception) {
                    Log.d("Mi2","${e.message}")

                    Toast.makeText(this.context, "Hubo algún error", Toast.LENGTH_LONG).show()
                }
            }, {
                // Error in request
//                    "Volley error: $it"
                Log.d("Mi2","$it")
            }

        ){
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val token =  preferences.getString("token", "hola")
                headers["Authorization"] = token!!
                return headers
            }}

        // Volley request policy, only one time request to
        // avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this.requireActivity())
            .addToRequestQueue(request)
    }
}
