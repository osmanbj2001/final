package com.example.final_movil.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.example.final_movil.*
import com.google.gson.Gson
import org.json.JSONObject

class AccountFragment : Fragment() {
    lateinit var txtVUsername :TextView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val txtV : TextView = view.findViewById(R.id.txtVLogout)
        txtVUsername = view.findViewById(R.id.txtVUserNameProfile)
        txtV.setOnClickListener {
            val intent= Intent(view.context, LoginRealEstate::class.java).apply {  }
            startActivity(intent)
        }
        getUser(view)
    }
    fun getUser( view: View)  {
        val preferences = this.requireActivity()
            .getSharedPreferences("data", Context.MODE_PRIVATE)
        val url = "https://real-red-goat-yoke.cyclic.app/users/find"
        val params = HashMap<String, String>()
        val id = preferences.getString("id", "hola")
        params["_id"] = id!!

        // Volley post request with parameters

        val request: JsonObjectRequest = object : JsonObjectRequest(
            Request.Method.POST,url, JSONObject(params as Map<*, *>?),
            { response ->
                // Process the json
                try {

                    val responseObject = Gson().fromJson(response.toString(), UserModel::class.java)
                    Log.d("User", responseObject.toString())
                    txtVUsername.text = responseObject.obj[0].username

                } catch (e: Exception) {
                    Log.d("Mi2","${e.message}")

                    Toast.makeText(this.context, "Hubo algún error", Toast.LENGTH_LONG).show()
                }
            }, {
                // Error in request
//                    "Volley error: $it"
                Log.d("Mi2","$it")
            }

        ){
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val token =  preferences.getString("token", "hola")
                headers["Authorization"] = token!!
                return headers
            }}

        // Volley request policy, only one time request to
        // avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this.requireActivity())
            .addToRequestQueue(request)
    }

}