package com.example.final_movil.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.example.final_movil.HouseDetails
import com.example.final_movil.HousesModel
import com.example.final_movil.R
import com.example.final_movil.VolleySingleton
import com.example.final_movil.adapter.MyAdapter
import com.google.gson.Gson
import org.json.JSONObject


class DashBoardFragment : Fragment() {
    lateinit var preferences: SharedPreferences
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?


    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dash_board2, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var swipe = view.findViewById<SwipeRefreshLayout>(R.id.swipe)
        var lvHouses = view.findViewById<ListView>(R.id.lvHouses)
        getHouses(lvHouses, view)



        swipe.setOnRefreshListener {
            Handler(Looper.getMainLooper()).postDelayed({
                Log.d("reloading", "hola")
                getHouses(lvHouses, view)
                swipe.isRefreshing =false
            },2000)

        }
    }

//    fun houses(lvHouses:ListView, view:View, house: HousesModel){
//        var list = mutableListOf<HousesModel>()
//        list.add(house)
//        lvHouses.adapter = MyAdapter(view.context,list)
//
//    }
    fun getHouses(lvHouses: ListView, view: View)  {
    preferences = this.requireActivity()
        .getSharedPreferences("data", Context.MODE_PRIVATE)

    val url = "https://real-red-goat-yoke.cyclic.app/houses/find"
//        var arrayAdapter: ArrayAdapter<*>
//        var houses = ArrayList<String>()
        var list = mutableListOf<HousesModel>()

        // Post parameters
        // Form fields and values
        val params = HashMap<String,String>()
        val jsonObject = JSONObject(params as Map<*, *>?)

        // Volley post request with parameters
        val request =object :JsonObjectRequest(
            Request.Method.POST,url, jsonObject,

            { response ->
                // Process the json

                try {
                    // Toast.makeText(this, "${response["message"]}", Toast.LENGTH_LONG).show()
                    val responseObject = Gson().fromJson(response.toString(), HousesModel::class.java)

                    for (house in responseObject.obj) {
                        list.add(responseObject)

                    }
                    lvHouses.adapter = MyAdapter(view.context,list)
                    lvHouses.setOnItemClickListener { _, _, position, _ ->

                        var idH = responseObject.obj[position].Id.toString()
                        val intent= Intent(view.context, HouseDetails::class.java).apply {  }
                        intent.putExtra("_id", idH)
                        startActivity(intent)
                    }


                } catch (e: Exception) {
                    Log.d("Mi2","${e.message}")

                    Toast.makeText(this.context, "Hubo algún error", Toast.LENGTH_LONG).show()
                }

            }, {
                // Error in request
//                    "Volley error: $it"
                Log.d("Mi2","$it")
            }
        ){
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                var token = preferences.getString("token", "hola")
                headers["Authorization"] = token!!
//                    "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBydWViYSIsImlhdCI6MTY3MDEwNzA4NX0.zqHSel3hgtZsInR1FUCGQGiCPNpBaEJLjcRb6Ko-3e0"
                return headers
            }}




    // Volley request policy, only one time request to
        // avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )


        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this.requireActivity())
            .addToRequestQueue(request)
    }

}