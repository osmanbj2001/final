package com.example.final_movil

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import org.json.JSONObject


class HouseDetails : AppCompatActivity() {
    lateinit var  txtTitle: TextView
    lateinit var txtPrice: TextView
    lateinit var txtSize: TextView
    lateinit var txtBath: TextView
    lateinit var txtBed: TextView
    lateinit var txtAddress: TextView
    lateinit var txtDescription: TextView
    lateinit var txtPhone:TextView
    lateinit var txtName:TextView
    lateinit var imgCasa:ImageView
    var id:String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_house_details)
        val mIntent = intent
         id = mIntent.getStringExtra("_id").toString()
        Log.d("casa", id)
        val dialogView = layoutInflater.inflate(R.layout.contactdialog, null)
        val dialog = AlertDialog.Builder(this).setView(dialogView).setTitle("Datos de contacto").create()
         txtTitle = findViewById(R.id.txtVTitleHouseD)
         txtPrice = findViewById(R.id.txtVPriceHouseD)
         txtSize = findViewById(R.id.txtVSizeHouseD)
         txtBath = findViewById(R.id.txtVBathHouseD)
         txtBed = findViewById(R.id.txtVBedHouseD)
         txtAddress = findViewById(R.id.txtVHouseAddress)
         txtDescription = findViewById(R.id.txtVHouseDescription)
         txtName = dialogView.findViewById(R.id.txtNameContactDialog)
         txtPhone = dialogView.findViewById(R.id.txtPhoneContactDialog)
         imgCasa = findViewById(R.id.imgVHouseDes)
            val btn:Button = findViewById(R.id.btnContactarDetail)
        getHouses()

        btn.setOnClickListener{
            dialog.show()

        }

    }
    fun getHouses()  {
        val preferences = getSharedPreferences("data", Context.MODE_PRIVATE)
        val url = "https://real-red-goat-yoke.cyclic.app/houses/find"

        // Post params to be sent to the server
        val params = HashMap<String, String>()
        params["_id"] = id

        // Volley post request with parameters
        val request =object : JsonObjectRequest(
            Request.Method.POST,url, JSONObject(params as Map<*, *>?),
            Response.Listener<JSONObject?>()
            { response ->
                // Process the json

                try {
                    val responseObject = Gson().fromJson(response.toString(), HousesModel::class.java)
                    Log.d("casaR", responseObject.toString() )
                    txtTitle.text = responseObject.obj[0].houseHeader.toString()
                    txtPrice.text = ("$" + responseObject.obj[0].price.toString())
                    txtSize.text = responseObject.obj[0].terrainArea.toString()
                    txtBath.text = responseObject.obj[0].bathrooms.toString()
                    txtBed.text = responseObject.obj[0].bedrooms.toString()
                    txtAddress.text = responseObject.obj[0].address.toString()
                    txtDescription.text = responseObject.obj[0].description.toString()
                    txtName.text = responseObject.obj[0].ownerName
                    txtPhone.text = responseObject.obj[0].ownerPhone
//                    Log.d("casa",responseObject.obj[0].image.toString())
                    Picasso.get().load(responseObject.obj[0].image[0]).into(imgCasa)

                } catch (e: Exception) {
                    Log.d("Mi2", "${e.message}")

                    Toast.makeText(this, "Hubo algún error", Toast.LENGTH_LONG).show()
                }

            }, {
                // Error in request
//                    "Volley error: $it"
                Log.d("Mi2","$it")
            }
        ){
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val token =  preferences.getString("token", "hola")
                headers["Authorization"] = token!!
                return headers
            }}



        // Volley request policy, only one time request to
        // avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            1, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )


        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this)
            .addToRequestQueue(request)
    }


}

